# `gqair`

![Screenshot](https://www.willemp.be/gqair/screenshots/screenshot-gqair-0.1.0-config.png)
![Screenshot](https://www.willemp.be/gqair/screenshots/screenshot-gqair-0.1.0-showtime.png)

Send and receive files without hassle, without cloud, without cables, and without
having to install an app on the other phone/computer.

This is the GUI version of [qair](https://codeberg.org/willempx/qair).

## Install
1. [Install rust](https://www.rust-lang.org/learn/get-started)
2. `sudo apt install libgtk-3-dev gcc`
3. `cargo install gqair`

