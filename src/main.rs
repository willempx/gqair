/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//! File transfer tool with graphical user interface.

extern crate gdk;
extern crate gtk;
extern crate qair;
extern crate qr2cairo;
use gtk::prelude::*;
use std::cell::RefCell;
use std::convert::TryInto;
use std::env;
use std::ops::Deref;
use std::path::PathBuf;
use std::rc::Rc;

const HOSTNAME_ON_AUTODETECT_FAILURE: &str = "127.0.0.1";

/// Start and show the graphical user interface.
fn main() {
    let gtk_ui = GtkUi::new();
    gtk_ui.show_ui()
}

#[derive(Clone)]
pub struct GtkUi {
    rc: Rc<GtkUiInternal>,
}

impl Deref for GtkUi {
    type Target = GtkUiInternal;
    fn deref(&self) -> &GtkUiInternal {
        &self.rc
    }
}

/// Typesafe alternative to avoid string-based glade.
///
/// glade (what we draw the UI in) is based on strings and casts, which we of course don't want.
/// So the first thing we do, is convert that to something with proper types - that's most of
/// this struct.
pub struct GtkUiInternal {
    send_file_chooser_button: gtk::FileChooserButton,
    receive_file_chooser_button: gtk::FileChooserButton,
    send_check_button: gtk::CheckButton,
    receive_check_button: gtk::CheckButton,
    start_button: gtk::Button,
    port_spin_button: gtk::SpinButton,
    top_stack: gtk::Stack,
    showtime_box: gtk::Box,
    qrcode_drawing_area: gtk::DrawingArea,
    url_label: gtk::Label,
    main_window: gtk::Window,
    copy_button: gtk::Button,
    listener: RefCell<Option<qair::Listening>>,
}

impl GtkUi {
    /// Come into existence.
    ///
    /// By limitations of gtk/glade, this unfortunately also has to do gtk initialization.
    pub fn new() -> Self {
        let gtk_ui = Self::populate();
        return gtk_ui;
    }

    /// Show ourself to the end-user (start drawing to screen etc).
    ///
    /// Blocking.
    pub fn show_ui(self) {
        self.connect();
        if let Ok(path) = env::current_dir() {
            self.receive_file_chooser_button.set_filename(path);
        }
        self.update_can_start();
        self.main_window.show_all();
        gtk::main();
    }

    /// Get rid of string-based-untyped-programming.
    ///
    /// By limitations of gtk/glade, this unfortunately also has to do gtk initialization.
    fn populate() -> Self {
        if gtk::init().is_err() {
            panic!("Failed to initialize GTK.");
        }
        static GLADE_UI_DEFINITION: &'static str = include_str!("../res/gtk-ui.glade");
        let builder: gtk::Builder = gtk::Builder::new_from_string(GLADE_UI_DEFINITION);
        let fields = GtkUiInternal {
            send_file_chooser_button: builder.get_object("send_file_chooser_button").unwrap(),
            receive_file_chooser_button: builder.get_object("receive_file_chooser_button").unwrap(),
            send_check_button: builder.get_object("send_check_button").unwrap(),
            receive_check_button: builder.get_object("receive_check_button").unwrap(),
            start_button: builder.get_object("start_button").unwrap(),
            port_spin_button: builder.get_object("port_spin_button").unwrap(),
            top_stack: builder.get_object("top_stack").unwrap(),
            showtime_box: builder.get_object("showtime_box").unwrap(),
            qrcode_drawing_area: builder.get_object("qrcode_drawing_area").unwrap(),
            url_label: builder.get_object("url_label").unwrap(),
            copy_button: builder.get_object("copy_button").unwrap(),
            main_window: builder.get_object("main_window").unwrap(),
            listener: RefCell::new(None),
        };
        return GtkUi {
            rc: Rc::new(fields),
        };
    }

    /// Which file does the user want to send.
    fn file_to_send(&self) -> Option<PathBuf> {
        if self.send_check_button.get_active() {
            self.send_file_chooser_button.get_filename()
        } else {
            None
        }
    }

    /// Where does the user wants to put the files uploaded to his/her computer?
    fn directory_to_receive_into(&self) -> Option<PathBuf> {
        if self.receive_check_button.get_active() {
            self.receive_file_chooser_button.get_filename()
        } else {
            None
        }
    }

    /// Whether we can `start_webserver`.
    fn can_start(&self) -> bool {
        return self.file_to_send() != None || self.directory_to_receive_into() != None;
    }

    /// Update GUI indicators on whether we can `start_webserver`.
    fn update_can_start(&self) {
        self.start_button.set_sensitive(self.can_start());
    }

    /// Update GUI to switch from configuring screen to fileserving screen.
    fn switch_to_showtime(&self, url: &str) {
        self.url_label.set_text(url);
        self.top_stack.set_visible_child(&self.showtime_box);
    }

    /// Shows a modal errorbox with given message. Blocking.
    fn show_error(&self, message: &str) {
        let message_box = gtk::MessageDialog::new(
            Some(&self.main_window),
            gtk::DialogFlags::MODAL,
            gtk::MessageType::Error,
            gtk::ButtonsType::Ok,
            message,
        );
        message_box.run();
        message_box.destroy();
    }

    /// Showtime! Run the HTTP server.
    ///
    /// On success, does not block (starts a new thread).
    /// On failure, show an error message dialog box.
    ///
    /// The only way to stop the HTTP server is to close the app.
    /// For deatils see https://docs.rs/iron/0.6.1/iron/struct.Listening.html#method.close
    ///
    /// Precondition: `can_start`.
    fn start_webserver(&self) {
        assert!(self.can_start());
        if let Some(dir) = self.directory_to_receive_into() {
            if let Err(error) = env::set_current_dir(&dir) {
                self.show_error(&format!("Error opening target directory: {}", error));
                return;
            }
        }

        let qair = self.create_qair();
        let url = match qair.url() {
            Ok(url) => url,
            Err(error) => {
                self.show_error(&format!("Cannot format URL: {}", error));
                return;
            }
        };

        let result = qair.start();
        match result {
            Ok(listener) => {
                self.listener.replace(Some(listener));
                self.switch_to_showtime(&url);
            }
            Err(error) => {
                if self.directory_to_receive_into() != None {
                    // Avoid user having to close the app to unmount the target dir's media.
                    let _ = env::set_current_dir("/");
                }
                self.show_error(&format!("Error starting http server: {}", error));
            }
        }
    }

    fn create_qair(&self) -> qair::Qair {
        let mut qair = qair::Qair::new();
        qair.set_file_to_send(&self.file_to_send());
        let receive = self.receive_check_button.get_active();
        qair.set_is_receiving_enabled(receive);
        qair.set_ip_autodetection_config(qair::IpAutodetectionConfig::AutoDetectWithFallback(
            HOSTNAME_ON_AUTODETECT_FAILURE.to_string(),
        ));
        let port = self.port_spin_button.get_value() as u16;
        qair.set_port(port);
        qair
    }

    /// Wire up GTK callbacks.
    fn connect(&self) {
        connect_sensitive_check_button(&self.send_check_button, &self.send_file_chooser_button);
        connect_sensitive_check_button(
            &self.receive_check_button,
            &self.receive_file_chooser_button,
        );
        let self_copy = self.clone();
        self.main_window.connect_delete_event(move |_, _| {
            if let Some(mut listener) = self_copy.listener.replace(None) {
                // This does not actually stop the webserver,
                // but it prevents preventing the app to close.
                let _ = listener.close();
            }
            gtk::main_quit();
            Inhibit(false)
        });
        let self_copy = self.clone();
        self.start_button
            .connect_clicked(move |_| self_copy.start_webserver());
        let self_copy = self.clone();
        self.send_check_button
            .connect_toggled(move |_| self_copy.update_can_start());
        let self_copy = self.clone();
        self.receive_check_button
            .connect_toggled(move |_| self_copy.update_can_start());
        let self_copy = self.clone();
        self.send_file_chooser_button
            .connect_file_set(move |_| self_copy.update_can_start());
        let self_copy = self.clone();
        self.receive_file_chooser_button
            .connect_file_set(move |_| self_copy.update_can_start());
        let self_copy = self.clone();
        self.qrcode_drawing_area
            .connect_draw(move |drawing_area, cr| {
                let width = drawing_area.get_allocated_width().try_into().unwrap();
                let height = drawing_area.get_allocated_height().try_into().unwrap();
                let text = self_copy.url_label.get_text().unwrap();
                let _ = qr2cairo::draw(cr, width, height, &text.as_str());
                Inhibit(false)
            });
        let self_copy = self.clone();
        self.copy_button.connect_clicked(move |_| {
            let clipboard = gtk::Clipboard::get(&gdk::SELECTION_CLIPBOARD);
            clipboard.set_text(&self_copy.url_label.get_text().unwrap());
        });
    }
}

/// Makes `target` sensitive/insitive when `check_button` is switched.
///
/// `target` becomes insensitive/disabled/grayed-out when `check_button` is off,
/// and sensitive/enabled/not-grayed-out when `check_button` is on.
fn connect_sensitive_check_button(
    check_button: &gtk::CheckButton,
    target: &gtk::FileChooserButton,
) {
    let target = target.clone();
    check_button.connect_toggled(move |check_button| {
        target.set_sensitive(check_button.get_active());
    });
}
